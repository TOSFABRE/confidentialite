Bienvenue sur ***Garde Pharma***.

Nous sommes heureux que vous ayez choisi d'utiliser nos produits et services (« Services »). Les présentes Conditions générales d'utilisation (« CGU ») régissent votre utilisation des Services et contiennent des informations importantes concernant nos Services et vos droits, devoirs et obligations liés à l'utilisation de nos Services. Ces CGU continueront de s'appliquer.

***Garde Pharma*** est une application développée par **Tosfabre Dev**, (« ***Garde Pharma*** », « Nous », « Notre »), vous permettant de géolocaliser les pharmacies et d'accéder à de nombreuses informations sur la santé et la liste des médicaments.

En utilisant ***Garde Pharma***, vous acceptez d'être lié-e par les présentes conditions générales.

***Garde Pharma*** s'engage à protéger votre vie privée et à vous offrir des services sûrs et fiables. Nous pouvons modifier ces Conditions de temps en temps. Si nous le faisons, nous publierons les modifications sur nos réseaux sociaux et dans l’application dans la rubrique dédiée et nous vous en informerons par e-mail sur demande dans un délai de 48 heures. Si vous ne souhaitez pas accepter ces modifications, vous devez cesser d'utiliser nos services. Sinon, en continuant à utiliser nos services, vous acceptez ces modifications.

Si vous utilisez ***Garde Pharma*** à des fins commerciales, vous devez accepter les Conditions d'utilisation supplémentaires.

## 1.	Contenu

Le Contenu sur l’application ***Garde Pharma*** peut inclure des textes et des images (ci-après le « Contenu »). Tout le contenu est fourni uniquement à des fins informatives et éducatives. Vous ne devez pas modifier, louer, vendre, diffuser, participer à une transaction ou à un transfert ou créer des œuvres dérivées tirées du contenu. Nous nous réservons le droit de modifier ou de supprimer le contenu à tout moment et à notre discrétion.

***Garde Pharma*** peut contenir des liens vers d'autres sites Web ou applications tiers qui ne sont pas sous le contrôle de ***Garde Pharma***. Nous ne sommes pas responsables des contenus ou des pratiques de ces autres sites. Il est de votre responsabilité de prendre connaissance des conditions d'utilisation et des politiques de confidentialité des autres sites.

## 2.	Garantie

Nous ne faisons aucune déclaration ni garantie quant à l'exactitude, la fiabilité, la qualité, la santé et la sécurité des services ***Garde Pharma***. Vous reconnaissez que ***Garde Pharma*** et son contenu sont fournis « en l’état » et «tel que disponible ».

## 3.	Limitations de responsabilité

En aucun cas ***Garde Pharma*** ne saurait être tenue responsable des pertes ou dommages indirects, spéciaux, consécutifs ou accessoires découlant de l'utilisation des services, y compris notamment, sans limitation, toutes pertes ou dommages causés par la perte de données ou de bénéfices, ou la négligence de ***Garde Pharma***.

En aucune circonstance, la responsabilité totale de ***Garde Pharma*** découlant de l'utilisation du service ***Garde Pharma*** ne dépassera pas le montant que vous avez payé pour l'utilisation des services.

## 4.	Divers
Les présentes CGU sont régies et interprétées conformément ***à la loi n° 2019-014 du 29 octobre 2019 relative à la protection des données à caractère personnel de la république togolaise*** sans égard aux principes de conflits de lois. Si une partie de ces CGU est jugée inexécutable ou inapplicable, les dispositions restantes resteront en vigueur. Rien dans ces CGU ne crée des droits en faveur de tiers.

## 5.	Contact
Si vous avez des questions au sujet de ces CGU, veuillez nous contacter par email ***Garde Pharma*** à l'adresse [tosfabre@gmail.com](mailto:tosfabre@gmail.com)
