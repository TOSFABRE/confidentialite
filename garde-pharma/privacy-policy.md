Cette politique de confidentialité s'applique à l'application mobile ***Garde Pharma*** (puisque Garde Pharma est une application pour l’instant) et fournit des informations au public sur les pratiques de confidentialité mises en œuvre par l'application. Nous reconnaissons l'importance de protéger la confidentialité de vos informations personnelles et nous sommes engagés à le faire.
 
Cette politique de confidentialité décrit les pratiques de collecte, d'utilisation et de divulgation des informations personnelles par le biais de cette application.

De ce fait, si vous utilisez l'application ***Garde Pharma***, vous acceptez et serez lié par les termes de cette politique de confidentialité.

## 1.	Généralité

***Garde Pharma*** est une application mobile destinée à aider à toute personne à géolocaliser, à trouver des médicaments et des informations pertinentes pour tout achat de produits. L'application collecte certaines informations sur les utilisateurices, afin de fournir les services. ***Garde Pharma*** traitera vos informations conformément à cette politique de confidentialité et à la loi applicable (Loi n° 2019-014 relative à la protection des données à caractère personnel)

## 2.	Collecte et traitement des données

Lors de l'utilisation de l'application ***Garde Pharma***, vous êtes invité-e à fournir l'information suivante :

•	Localisation 

Nous utilisons cette information pour fournir des services et gérer votre navigation et vous envoyer des informations pertinentes. Les informations personnelles peuvent être partagées avec des tiers fournisseurs de services, y compris des fournisseurs de services sécurisés, des développeurs, des sponsors d'offres et des partenaires. Les informations sont également partagées avec des tiers pour des fins de marketing ou à des fins publicitaires

## 3.	Sécurité

***Garde Pharma*** a mis en place des procédures de sécurité pour protéger les informations personnelles d'utilisation. Les informations sont conservées sur des serveurs sécurisés et sont protégées par des mesures physiques, électroniques et administratives

## 4.	Droits d’utilisation 

En cas de questions ou pour plus d'informations sur la politique de confidentialité, veuillez envoyer un e-mail à l'équipe de ***Garde Pharma*** à l'adresse suivante : [tosfabre@gmail.com](mailto:tosfabre@gmail.com).

Les utilisateurices ont le droit de modifier ou de supprimer leurs informations à tout moment.
Les utilisateurices peuvent de plus demander à ce que leurs informations soient modifiées, supprimées ou limitées dans l'utilisation qu'en fait ***Garde Pharma*** et de demander à ce que cela soit faite. Cette demande prend un délai maximum de 05 jours ouverts. Les utilisateurices peuvent également s’opposer au traitement des informations, et demander à recevoir les informations sous forme d’une copie électronique. Les demandes doivent être adressées à l'adresse suivante et prendront un délai maximum de 15 jours : [tosfabre@gmail.com](mailto:tosfabre@gmail.com).

## 5.	Changements apportés à la politique de confidentialité

***Garde Pharma*** peut modifier cette politique de temps en temps pour refléter l'évolution des pratiques de confidentialité. Les modifications seront affichées sur cette page et entrées en vigueur immédiatement. Nous vous encourageons à vérifier cette page régulièrement pour vous assurer que vous êtes toujours au courant des modifications apportées. 

